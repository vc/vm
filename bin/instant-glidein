#!/bin/bash
#
#  Copyright (c) CERN 2016
#
#  Licensed under the Apache License, Version 2.0
#
#  Author: Laurence Field
#

logging_functions="/cvmfs/grid.cern.ch/vc/bin/logging_functions"
if [ -e ${logging_functions} ]; then
    . ${logging_functions}
else
    echo "ERROR Could not source logging functions from ${logging_functions}." 1>&2
fi

shared_dir="/var/lib/boinc/shared"

function link_log(){
    while true; do
	if [ -e $1 ]; then
	    ln -f $1 $2
	    break
	fi 
	sleep 5 
    done
}

for file in 10_security.config 14_network.config 20_workernode.config 30_lease.config 40_ccb.config; do
    cp /cvmfs/grid.cern.ch/vc/etc/condor/config.d/${file} /etc/condor/config.d/
done
ln -sf /cvmfs/grid.cern.ch/vc/bin/job-wrapper /usr/local/bin/
ln -sf /cvmfs/grid.cern.ch/vc/bin/multicore-shutdown /usr/local/bin/
rm -f /etc/condor/config.d/50cernvm
echo > /etc/condor/condor_config.local
sed -i s"/^Resource_Lease_Start_Time = .*/Resource_Lease_Start_Time = $(date -u +%s )/" /etc/condor/config.d/30_lease.config
cp /tmp/x509up_u0 /tmp/x509up_u99
chown nobody:nobody /tmp/x509up_u99

RESULT=$(xmllint /var/lib/boinc/shared/init_data.xml | grep "^<result_name" | cut -d ">" -f2 | cut -d "<" -f1)

log_debug "HTCondor ping"
for run in {1..3}; do
    _condor_SEC_CLIENT_AUTHENTICATION_METHODS=GSI condor_ping -debug -verbose -type COLLECTOR DC_NOP >/tmp/stdout 2>/tmp/stderr
    result=$?
    log_debug "$result"
    if [ ! ${result} -eq 0 ]; then
        if [ $run -eq 3 ]; then
	    log_debug "$(cat /tmp/stdout /tmp/stderr)"
	    tar -czf /tmp/${RESULT}.tgz -C /var/log/condor/ $(ls /var/log/condor)
	    davix-put -k --key /tmp/x509up_u0 --cert /tmp/x509up_u0 /tmp/${RESULT}.tgz https://data-bridge.cern.ch/myfed/cms-output/logs/${RESULT}.tgz >/tmp/upload.out 2>/tmp/upload.err
	    boinc-shutdown 206 "Could not ping HTCondor."
	    exit 1
	fi
    else
	break
    fi
done

pid_file="/var/run/condor/condor_master.pid"

/usr/sbin/condor_master -f -pidfile ${pid_file} &
link_log /var/log/condor/MasterLog /var/www/html/logs/MasterLog &
link_log /var/log/condor/StartLog /var/www/html/logs/StartLog &
link_log /var/log/condor/StarterLog /var/www/html/logs/StarterLog &

if [ $(nproc) -gt 1 ]; then 
    for i in $(seq 1 $(nproc)); do
	link_log /var/log/condor/StarterLog.slot${i} /var/www/html/logs/StarterLog.slot${i} &
    done
fi

sleep 5 #Need to wait for pid file to be created
if [ -e ${pid_file} ]; then
    wait $(cat ${pid_file})
fi

#return_value=$?
return_value='N/A'

if [ ! -e /var/www/html/logs/finished_1.log ]; then
    tar -czf /tmp/${RESULT}.tgz -C /var/www/html/logs/ $(ls /var/www/html/logs/)
    davix-put -k --key /tmp/x509up_u0 --cert /tmp/x509up_u0 /tmp/${RESULT}.tgz https://data-bridge.cern.ch/myfed/cms-output/logs/${RESULT}.tgz >/tmp/upload.out 2>/tmp/upload.err
    START=$(grep ^Resource_Lease_Start_Time /etc/condor/config.d/30_lease.config | cut -d '=' -f 2)
    END=$(date -u +%s )
    DIFF=$(( ${END} - ${START} ))
    if [ ${DIFF} -lt 660 ]; then
	echo "Did the tarball get created?" | /sbin/vboxmonitor
	ls /tmp/${RESULT}.tgz | /sbin/vboxmonitor
	echo "Here is the upload output" | /sbin/vboxmonitor
	cat /tmp/upload.out | /sbin/vboxmonitor
	echo "Here is the upload error" | /sbin/vboxmonitor
	cat /tmp/upload.err | /sbin/vboxmonitor
	echo "Here is the condor directory" | /sbin/vboxmonitor
	ls /var/log/condor/ | /sbin/vboxmonitor
	echo "Here is the MasterLog" | /sbin/vboxmonitor
	cat /var/log/condor/MasterLog | /sbin/vboxmonitor
	echo "Here is the KernelTuning.log" | /sbin/vboxmonitor
	cat /var/log/condor/KernelTuning.log | /sbin/vboxmonitor
	echo "Here is the StartLog" | /sbin/vboxmonitor
	cat /var/log/condor/StartLog | /sbin/vboxmonitor
	
	boinc-shutdown 207 "No jobs were available to run."
    fi
else
    if [ -e /tmp/mshutdown ]; then
	log_info "Multicore Shutdown: Idle > Busy ( $(cat /tmp/mshutdown) )"
    fi
    grep 300 /tmp/exit_status  >/dev/null 2>&1
    if [ $? -eq 0 ] && [ $(grep 00 /tmp/exit_status | sort -u | wc -l ) -eq 1 ]; then
	boinc-shutdown 207 "No jobs were available to run."
    else	
	boinc-shutdown 0 "Condor exited with return value ${return_value}."
    fi
fi

boinc-shutdown 1 "Condor ended by dragons."

rm -rf /var/www/html/logs
