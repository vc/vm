#!/bin/bash

function bs_cms_configure_cvmfs_repositories {
    # Add app specific repositories
    # Use FQRNs since the names from here will be printed to the console/logfile
    bs_repositories="grid.cern.ch,cms.cern.ch,cms-ib.cern.ch,oasis.opensciencegrid.org,singularity.opensciencegrid.org"
    sed -e "s/^CVMFS_REPOSITORIES=.*/CVMFS_REPOSITORIES=${bs_repositories}/" -i /etc/cvmfs/default.local

    # activate the new CVMFS configuration
    log_info "Reloading and probing the CVMFS configuration"
    # Hint for impatient volunteers watching the console
    echo "May take a while. Be patient..."
    if ! cvmfs_config reload >/tmp/stdout 2>&1; then
        /sbin/vboxmonitor </tmp/stdout
        boinc-shutdown 206 "Reloading the CVMFS configuration failed"
        exit 0
    fi


    function probe_cvmfs_repos {
        cvmfs_config probe "$1" >/dev/null 2>&1
        result="$?"
        echo "${result}" >"${my_tmp_dir}/result_probe_$1"

        # output from cvmfs_config probe can't be used directly as it prints results delayed
        if [[ "${result}" == "0" ]]; then
            echo "[INFO] Probing /cvmfs/$1... OK"
        else
            echo "[ERROR] Probing /cvmfs/$1... Failed!"
        fi
    }
    # must be exported to be available for child shells
    export -f probe_cvmfs_repos


    # The following else branch requires vboxmonitor to be available.
    # In case of VM deployment, vboxmonitor is unavailable (see above).
    # => use a plain cvmfs_config probe instead.
    #
    # The else branch runs probes to all configured repos concurrently.
    # This intentionally puts a short high load peak on the network (LAN/internet)
    # Very weak networks may cause a task to fail here.
    # This seems to be better than to fail later when lots of work has been done backend.
    # Slow (high latency) but robust networks may benefit from the parallel probes and safe some time.

    if [[ "${flag_vm_deployment}" == "yes" ]]; then
        cvmfs_config probe
        cvmfs_config stat grid.cern.ch
    else
        my_umask="$(umask)"
        umask 077
        export my_tmp_dir="$(mktemp -d)"
        umask "${my_umask}"

        /sbin/vboxmonitor < <(
            for bs_repo in ${bs_repositories//,/ }; do
                probe_cvmfs_repos "${bs_repo}" &
            done
            wait) # until all repos are probed and the results are processed by vboxmonitor

        if grep -v '^0$' <(cat "${my_tmp_dir}/result_probe_"* 2>&1) >/dev/null 2>&1; then
            boinc-shutdown 206 "Probing the CVMFS repositories failed"
            exit 0
        fi

        # prints a hint to the console and the logfile whether openhtc.io and/or a local proxy is used.
        cvmfs_excerpt=($(cut -d ' ' -f 1,17,18 < <(tail -n1 < <(cvmfs_config stat grid.cern.ch))))
        log_info "Excerpt from \"cvmfs_config stat\": VERSION HOST PROXY"
        log_info "$(echo "${cvmfs_excerpt[0]} ${cvmfs_excerpt[1]%"/cvmfs/grid.cern.ch"} ${cvmfs_excerpt[2]}")"

        rm -frd "${my_tmp_dir}" &
    fi
}
